from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


class UserLoginForm(forms.Form):
    
    username = forms.RegexField(label=("Логин"), max_length=30, regex=r'^[\w.@+-]+$')
    password = forms.CharField(label="Пароль",widget=forms.PasswordInput)
    
class UserRegistrationForm(UserCreationForm):
    username = forms.RegexField(label=("Логин"), max_length=30, regex=r'^[\w.@+-]+$')
    email = forms.RegexField(label=("Почта"), max_length=30, regex=r'^[\w.@+-]+$')

    password1 = forms.CharField(label="Пароль", widget=forms.PasswordInput)
    password2 = forms.CharField(
        label="Подтверждение пароля",
        widget=forms.PasswordInput)
    
        # email = forms.EmailField(label=_("Email"), max_length=254)

    class Meta:
        model = User
        fields = ['email', 'username', 'password1', 'password2']
        
    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if User.objects.filter(email=email).exclude(username=username):
            raise forms.ValidationError(u'должен быть уникальным')
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if not password1 or not password2:
            raise ValidationError("подтверждение пароля")
        
        if password1 != password2:
            raise ValidationError("пароли должны совпадать")
        
        return password2
    
