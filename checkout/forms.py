from django import forms
from .models import Order

class MakePaymentForm(forms.Form):

    MONTH_CHOICES = [(i, i) for i in range(1, 12)]
    YEAR_CHOICES = [(i, i) for i in range(2020, 2036)]

    credit_card_number = forms.CharField(label='Номер карты', required=False)
    cvv = forms.CharField(label='Код (CVV)', required=False)
    expiry_month = forms.ChoiceField(label='Месяц', choices=MONTH_CHOICES, required=False)
    expiry_year = forms.ChoiceField(label='Год', choices=YEAR_CHOICES, required=False)
    stripe_id = forms.CharField(widget=forms.HiddenInput)


class OrderForm(forms.ModelForm):
    
    full_name = forms.RegexField(label=("ФИО"),regex=r'^[\w.@+-]+$')
    phone_number= forms.RegexField(label=("Тел. номер"),regex=r'^[\w.@+-]+$')
    country= forms.RegexField(label=("Страна"),regex=r'^[\w.@+-]+$')
    postcode= forms.RegexField(label=("Индекс почты"),regex=r'^[\w.@+-]+$')
    city= forms.RegexField(label=("Город"),regex=r'^[\w.@+-]+$')
    street_address1= forms.RegexField(label=("Адресс"),regex=r'^[\w.@+-]+$')
    street_address2= forms.RegexField(label=("Адресс2"),regex=r'^[\w.@+-]+$')
    state= forms.RegexField(label=("Область"),regex=r'^[\w.@+-]+$')
    class Meta:
        model = Order
        fields = ('full_name', 'phone_number', 'country', 'postcode', 'city', 'street_address1', 'street_address2', 'state')